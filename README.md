# Information

[![Stars](https://img.shields.io/gitea/stars/DontBlameMe/Wallpapers?gitea_url=https%3A%2F%2Fcodeberg.org&style=for-the-badge&labelColor=%231e1e2e&color=%23f9e2af)](https://codeberg.org/DontBlameMe/Wallpapers/stars)
[![Forks](https://img.shields.io/gitea/forks/DontBlameMe/Wallpapers?gitea_url=https%3A%2F%2Fcodeberg.org&style=for-the-badge&labelColor=%231e1e2e&color=%2394e2d5)](https://codeberg.org/DontBlameMe/Wallpapers/forks)
[![NoGithub](https://img.shields.io/badge/Github-No-static?style=for-the-badge&labelColor=%231e1e2e&color=%23f38ba8)](https://nogithub.codeberg.page)

The minimum resolution of these wallpapers is 3840x2160 (4K).

# Used Websites

- [UHDPaper](https://uhdpaper.com)
- [Pexels](https://pexels.com)
- [Freepik](https://freepik.com)
- [BasicAppleGuy](https://basicappleguy.com/haberdashery/graffiti)
